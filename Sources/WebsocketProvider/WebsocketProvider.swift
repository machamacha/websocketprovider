import WebSocket

public struct WebsocketAddress {
    let scheme: String
    let host: String
    let port: UInt16
    let endpoint: String
    public init(scheme: String, host: String, port: UInt16, endpoint: String) {
        self.scheme = scheme
        self.host = host
        self.port = port
        self.endpoint = endpoint
    }

    var fullAddress: String {
        var address: String = ""
        if !scheme.isEmpty {
            address += "\(scheme)://"
        }
        address += host
        if port > 0 {
            address += ":\(port)"
        }
        if !endpoint.isEmpty {
            address += "/\(endpoint)"
        }
        return address
    }
}

public protocol WebsocketProtocol: class {
    var isConnected: Bool { get }
    func send(_ text: String) throws
    var onText: ((WebsocketProtocol, String) -> ())? { get set }
    var onData: ((WebsocketProtocol, Data) -> ())? { get set }
    var onClose: ((WebsocketProtocol, String) -> ())? { get set }
    func send(_ data: Data) throws
}

public class WebsocketVapor: WebsocketProtocol {
    private let websocket: WebSocket
    init(websocket: WebSocket) {
        self.websocket = websocket
    }
    public fileprivate(set) var isConnected: Bool = false
    public func send(_ text: String) throws {
        websocket.send(text)
    }

    public func send(_ data: Data) throws {
        websocket.send(data)
    }
    public var onText: ((WebsocketProtocol, String) -> ())?
    public var onData: ((WebsocketProtocol, Data) -> ())?
    public var onClose: ((WebsocketProtocol, String) -> ())?
}

public protocol WebsocketProvider {
    func connect(to address: WebsocketAddress, maxFrameSize: Int, completion: @escaping (WebsocketProtocol) -> ()) throws
}

public class WebsocketVaporImpl: WebsocketProvider {
    public init() {}
    public func connect(to address: WebsocketAddress, maxFrameSize: Int, completion: @escaping (WebsocketProtocol) -> ()) throws {
        let uri = address.fullAddress


        let worker = MultiThreadedEventLoopGroup(numberOfThreads: 1)
        let ws = try HTTPClient.webSocket(hostname: uri, port: Int(address.port), maxFrameSize: maxFrameSize, on: worker).wait()

        let websocketImpl = WebsocketVapor(websocket: ws)

        ws.onText { ws, text in
            websocketImpl.isConnected = true
            websocketImpl.onText?(websocketImpl, text)
        }
        ws.onBinary { ws, data in
            websocketImpl.isConnected = true
            websocketImpl.onData?(websocketImpl, data)
        }
        ws.onCloseCode { code in
            websocketImpl.isConnected = false
            websocketImpl.onClose?(websocketImpl, String(describing: code))
        }
        ws.onError { (ws, error) in
            print("error: \(error)")
        }
        websocketImpl.isConnected = true
        completion(websocketImpl)
    }
}
