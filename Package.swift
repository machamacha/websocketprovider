// swift-tools-version:4.1
import PackageDescription

let package = Package(
    name: "WebsocketProvider",
    products: [
        .library(name: "WebsocketProvider", targets: ["WebsocketProvider"])
    ],
    dependencies: [
        .package(url: "https://github.com/vapor/websocket.git", from: "1.0.0")
    ],
    targets: [
        .target(name: "WebsocketProvider", dependencies: ["WebSocket"])
    ]
)
